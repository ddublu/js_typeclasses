// nameless lambda only recursion

const y = le => 
  { return f =>
    { return f(f)
    }; f =>
    { return le(
      x => ((f(f))(x));
      );
    };
  };
  

// n^n as yCombinator
const n = 2;
const powerN = le =>
  { return f =>
    { return f(f)
    }; f =>
    { return le(
      n => ((f(f))(n*n));
      );
    };
  };
// fib(n) = [n + (n - 1) + (n - 2)...]
let n = 1
  , lastN
  , nextN
  ;

let fib = n =>
  { if ( n = 1 )
    { return 
    { lastN = 1 }
    ; else
    { lastN = n
    ; n = 
    let nextN = n + lastN
// fibonnaci as yCombinator
let n = 1;
const fib = n => 
  { return (f(n)) =>  
    { return f => 
      { n = n + n };
    };
  };